package com.bobbuy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bobbuy.repository.ShopRepository;




// Title : IndexController
// Description : 최초 서버 접속시 해당 뷰와 매핑
@Controller
public class IndexController {	
	
	@Autowired
	ShopRepository shopRepository;
	
	@RequestMapping(value = "/")
	public ModelAndView indexPage(){
		ModelAndView mav = new ModelAndView("index");
		System.out.println("Bobbuy Server Start!");
		
		
		
		return mav;
	}
}
