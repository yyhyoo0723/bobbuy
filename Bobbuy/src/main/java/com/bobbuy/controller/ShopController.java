package com.bobbuy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sun.misc.BASE64Encoder;

import com.bobbuy.domain.Shop;
import com.bobbuy.service.ShopService;

@RestController
public class ShopController {

	@Autowired
	private ShopService shopService;

	@RequestMapping(value = "/getShopByBc", method = RequestMethod.POST)
	public Shop getShopByBt(@RequestParam("mac") String mac) {

		System.out.println("PrinterController Start : /getShopByBc");
		System.out.println("request mac : " + mac);
		
		Shop shop = null;
		
		shop = new Shop();
		shop.setAddress("서울시 노원구 월계동");
		shop.setName("무선네트워크연구실");
		shop.setPhone("123-3456");
		
		
		shopService.save(shop);
		
		
		//System.out.println("response user getType : " + printerInfo.getType());

		return shop;
	}


}
