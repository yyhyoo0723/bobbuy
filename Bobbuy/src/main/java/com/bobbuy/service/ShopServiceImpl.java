package com.bobbuy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bobbuy.domain.Shop;
import com.bobbuy.repository.ShopRepository;

@Service
@Transactional(readOnly = true)
public class ShopServiceImpl implements ShopService {
	@Autowired
	private ShopRepository shopRepository;

	@Override
	@Transactional(readOnly = false)
	public void save(Shop shop) {
		// TODO Auto-generated method stub
		shopRepository.save(shop);
	}

}
