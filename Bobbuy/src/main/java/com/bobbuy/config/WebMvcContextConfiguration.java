package com.bobbuy.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

// 스마트프린터 xml버전 기준으로, 이 클래스에 applicationContext.xml 내용을 넣어야 됨. 루트 콘텍스트에 해당함.
// 그런데 servlet-context에 넣는 리소스나 뷰리졸버도 여기에서 하는듯? 그냥 여기에 함.

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.bobbuy" })	// 설정한 package를 스캔해서 콤포넌트를 등록. 예를 들면 Controller
public class WebMvcContextConfiguration extends WebMvcConfigurerAdapter {
	
	// 리소스 관리 설정
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/resources/");
	}
	
	// 메시지 컨버터 설정
	// 여기서 applicationContext.xml의 메시지 컨버터를 넣는걸 구현했는데, 잘 되는지 모르겠음. 확인해야됨.
	// 옛날 설정에 따르면 ByteArrayHttpMessageConverter랑 MappingJacksonHttpMessageConverter가 필요함.
	// 이거 관련 내용 https://groups.google.com/forum/#!topic/ksug/Btl3wh6GyBg 여기에 코드 있음
	@Override
	public void configureMessageConverters(
			List<HttpMessageConverter<?>> converters) {
		// TODO Auto-generated method stub
		// ByteArray이거 기본아님? http는 아닌가?
		//converters.add(new ByteArrayHttpMessageConverter()); 
		MappingJackson2HttpMessageConverter converter = createJsonConverter();
        converters.add(converter);
	}
    
	
    @Bean
    MappingJackson2HttpMessageConverter createJsonConverter() {
    	ObjectMapper objectMapper = new ObjectMapper();
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter(objectMapper);
    	return jsonConverter;
    }
	
	// 뷰 리졸버 선택
	// 1. 뷰리졸버 순위 : 2
	// 2. Prefix : WEB-INF/views/
	// 3. Suffix : .jsp
	
	
	@Bean
	public ViewResolver viewResolver(){
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setOrder(2);
		internalResourceViewResolver.setPrefix("WEB-INF/views/");
		internalResourceViewResolver.setSuffix(".jsp");
		return internalResourceViewResolver;
	}
	
	
}
