package com.bobbuy.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.bobbuy" }) // 설정한 package를 스캔해서 콤포넌트를 등록. 예를 들면 Controller. 이거 다른거랑 겹치면 안되나?
public class InfrastructureContextConfiguration {

	@Autowired
	private DataSource dataSource;

	
	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Bean
	public FactoryBean<EntityManagerFactory> entityManagerFactory() {
		// LocalContainerEntityManagerFactoryBean을 사용해서 persistence.xml에 있는 설정부를 이쪽 코드로 옮김
		LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
		emfb.setDataSource(this.dataSource);
		emfb.setJpaVendorAdapter(jpaVendorAdapter());
		return emfb;
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		return new HibernateJpaVendorAdapter();
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(this.entityManagerFactory);
		txManager.setDataSource(this.dataSource);
		return txManager;
	}
	
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://223.194.30.135:3306/bobbuy?characterEncoding=UTF-8");
		dataSource.setUsername("root");
		dataSource.setPassword("1q2w3e4r!");
		return dataSource;
	}

}
